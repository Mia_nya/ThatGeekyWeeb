<img align="left" alt="Metrics" src="https://raw.githubusercontent.com/ThatGeekyWeeb/ThatGeekyWeeb/master/github-metrics.svg">

<sup>🔌 [Discord](https://dsc.bio/thatweeb)  
[Fediverse (Pleroma)](https://disqcordia.space/Mia)<sup>

---
```text
  _  _             _  _             /\//\/|
 | \| |_  _ __ _  | \| |_  _ __ _  |/\|/\/ 
 | .` | || / _` | | .` | || / _` |         
 |_|\_|\_, \__,_| |_|\_|\_, \__,_|         
       |__/             |__/                
```
---

- [Spotbash](https://github.com/thatgeekyweeb/Spotbash) - POSIX Spotify controller
- [Fedish](https://github.com/thatgeekyweeb/Fedish) - POSIX sh fediverse client, because pain
- [Blobash](https://github.com/thatgeekyweeb/blobash) - POSIX sh Blobmoji CSS generator for Discord.
- [JsonPosix](https://github.com/thatgeekyweeb/jp) - POSIX sh json handler, because I'm insane
- [SquidtifyJS](https://github.com/ThatGeekyWeeb/SquidtifyJS) - Personalized version of Spotify's web player - Barely works

---
  - To Come:
    - shfire: POSIX sh discord client
    - basefire: POSIX sh discord storage
    - discord-fs-arikawa: [discord-fs](https://github.com/jonas747/discord-fs) revival and port to arikawa
